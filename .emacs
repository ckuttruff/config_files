;; There are some other config options in my emacs startup file, but nothing
;;  particularly interesting or worth saving

(custom-set-variables
 '(inhibit-startup-screen t)
 '(initial-scratch-message "")
 '(auto-save-file-name-transforms '((".*" "~/.emacs.d/autosaves/\\1" t)))
 '(backup-directory-alist '((".*" . "~/.emacs.d/backups/"))))

(custom-set-faces)

(setq-default indent-tabs-mode nil)
;(add-hook 'before-save-hook 'delete-trailing-whitespace)
(global-set-key (kbd "C-c s") 'delete-trailing-whitespace)
(setq-default require-final-newline t)
(setq-default show-trailing-whitespace t)

;; This was adapted from http://ergoemacs.org/emacs/elisp_examples.html
(defun switch-user-buffer (direction)
  "Switch user buffer"
  (let ((buf-fn (if (equal direction "left") 'previous-buffer 'next-buffer )))
    (eval `(,buf-fn))
    (let ((i 0))
      (while (and (string-match "^*" (buffer-name))
                  (< i 100)
                  (not (equal "*nrepl*" (buffer-name))))
        (setq i (1+ i))
	(eval `(,buf-fn))))))
(defun previous-user-buffer ()
  (interactive)
  (switch-user-buffer "left"))
(defun next-user-buffer ()
  (interactive)
  (switch-user-buffer "right"))
;Set keybindings for buffer switching                                                                                        
(global-set-key (kbd "C-c p") 'previous-user-buffer)
(global-set-key (kbd "C-c n") 'next-user-buffer)

